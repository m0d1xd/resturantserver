package com.example.m0d1x.universityresturantserver.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m0d1x.universityresturantserver.Activities.Tracking.TrackingOrder;
import com.example.m0d1x.universityresturantserver.Common.Common;
import com.example.m0d1x.universityresturantserver.Interface.ItemClickListener;
import com.example.m0d1x.universityresturantserver.R;
import com.example.m0d1x.universityresturantserver.Remote.APIService;
import com.example.m0d1x.universityresturantserver.ViewHolder.OrderViewHolder;

import com.example.m0d1x.universityresturantserver.model.NotificationModels.FCMResponse;
import com.example.m0d1x.universityresturantserver.model.NotificationModels.Notification;
import com.example.m0d1x.universityresturantserver.model.Request;
import com.example.m0d1x.universityresturantserver.model.NotificationModels.Sender;
import com.example.m0d1x.universityresturantserver.model.NotificationModels.Token;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

public class OrderStatus extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private FirebaseRecyclerAdapter<Request, OrderViewHolder> adapter;

    private FirebaseDatabase database;
    private DatabaseReference requests;


    private TextView tv_totalPrice;
    private Button btn_placeOrder;
    private MaterialSpinner spinner;
    private APIService mServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        //Inir FireBase
        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");
        mServices = Common.getFCMService();
        recyclerView = (RecyclerView) findViewById(R.id.listOrders);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        loadOrders();

    }

    private void loadOrders() {
        adapter = new FirebaseRecyclerAdapter<Request, OrderViewHolder>(
                Request.class,
                R.layout.order_layout,
                OrderViewHolder.class,
                requests) {
            @Override
            protected void populateViewHolder(OrderViewHolder orderViewHolder, final Request request, final int position) {
                orderViewHolder.tv_OrderID.setText(adapter.getRef(position).getKey());
                orderViewHolder.tv_OorderStatus.setText(Common.convertCodeToString(request.getStatus()));
                orderViewHolder.tv_Oorder_Address.setText(request.getAddress().toString());
                orderViewHolder.tv_Order_Phone.setText(request.getName());
                orderViewHolder.btn_Edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showUpdateDialog(adapter.getRef(position).getKey(), adapter.getItem(position));
                    }
                });
                orderViewHolder.btn_Details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent orderDetails = new Intent(OrderStatus.this, OrderDetails.class);
                        Common.CurrentRequest = request;
                        orderDetails.putExtra("OrderID", adapter.getRef(position).getKey());
                        startActivity(orderDetails);
                    }
                });
                orderViewHolder.btn_Directions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent Tracking = new Intent(OrderStatus.this, TrackingOrder.class);
                        Common.CurrentRequest = request;
                        startActivity(Tracking);
                    }
                });

                orderViewHolder.btn_Remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteOrder(adapter.getRef(position).getKey());
                    }
                });

            }
        };
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.UPDATE)) {
            showUpdateDialog(adapter.getRef(item.getOrder()).getKey(), adapter.getItem(item.getOrder()));

        } else if (item.getTitle().equals(Common.DELETE)) {
            deleteOrder(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);
    }

    private void showUpdateDialog(final String key, final Request item) {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(OrderStatus.this);
        alertDialog.setTitle("UpdateOrder");
        alertDialog.setMessage("Chose Status");

        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View view = layoutInflater.inflate(R.layout.update_order_status_layout, null);

        spinner = (MaterialSpinner) view.findViewById(R.id.spinner);
        spinner.setItems("Placed", "On My Way", "Shipped");
        alertDialog.setView(view);
        final String localkey = key;
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        item.setStatus(String.valueOf(spinner.getSelectedIndex()));
                        requests.child(localkey).setValue(item);

                        sendOrderStatusNotificationToUser(localkey, item);

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    private void sendOrderStatusNotificationToUser(final String Key, final Request item) {
        DatabaseReference tokens = database.getReference("Tokens");
        tokens.orderByKey().equalTo(item.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postDatasnapShot : dataSnapshot.getChildren()) {
                            Token token = postDatasnapShot.getValue(Token.class);
                            //Making raw Payload
                            Notification notification = new Notification("Your order " + Key + " was updated. ", "Resturant");
                            Sender content = new Sender(token.getToken(), notification);
                            mServices.sendNotification(content).enqueue(new Callback<FCMResponse>() {
                                @Override
                                public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                                    if (response.body().success == 1) {
                                        Toast.makeText(OrderStatus.this, "Order Was updated", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(OrderStatus.this, "Order Was updated ,but faild to send notification",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<FCMResponse> call, Throwable t) {
                                    Log.e("ERROR", "onFailure: " + t.getMessage());
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public void deleteOrder(String key) {
        requests.child(key).removeValue();
        adapter.notifyDataSetChanged();
    }

    public String getfoodImage(String foodId) {
        DatabaseReference db = FirebaseDatabase.getInstance().getReference("Foods");

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return null;

    }
}
