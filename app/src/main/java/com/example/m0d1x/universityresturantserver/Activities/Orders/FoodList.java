package com.example.m0d1x.universityresturantserver.Activities.Orders;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import info.hoang8f.widget.FButton;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m0d1x.universityresturantserver.Activities.HomeActivity;
import com.example.m0d1x.universityresturantserver.Common.Common;
import com.example.m0d1x.universityresturantserver.Interface.ItemClickListener;
import com.example.m0d1x.universityresturantserver.R;
import com.example.m0d1x.universityresturantserver.ViewHolder.FoodViewHolder;
import com.example.m0d1x.universityresturantserver.model.Food;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import java.util.UUID;

public class FoodList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseDatabase database;
    private DatabaseReference FoodList;
    private String CatID = "";
    private StorageReference storageReference;
    private FirebaseStorage storage;
    private TextView et_UploadStatus;
    private FirebaseRecyclerAdapter<Food, FoodViewHolder> adapter;
    private FloatingActionButton fab;
    Food newFood;
    MaterialEditText edtName, etDiscount, etPrice, etDiscription;
    FButton btn_select, btn_upload;
    static final int PICK_IMAGE_REQUEST = 71;
    //Search Functionallity
    private Uri saveUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        //Init Floating Button
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddFoodDialog();
            }
        });

        //Init FirebaseDB
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        database = FirebaseDatabase.getInstance();
        FoodList = database.getReference("Foods");

        recyclerView = (RecyclerView) findViewById(R.id.recycle_food_menu);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //Getting User last sellection For Catagory
        if (getIntent() != null) {
            CatID = getIntent().getStringExtra("MenuId");
        }
        if (!CatID.isEmpty() && CatID != null) {
            LoadItems(CatID);
        }
    }

    private void showAddFoodDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Add new Category");
        LayoutInflater inflater = this.getLayoutInflater();
        View add_food_layout = inflater.inflate(R.layout.add_new_food_layout, null);

        edtName = add_food_layout.findViewById(R.id.et_NewFoodName);
        etDiscription = add_food_layout.findViewById(R.id.et_Description);
        etPrice = add_food_layout.findViewById(R.id.et_Price);
        etDiscount = add_food_layout.findViewById(R.id.et_Discount);
        btn_select = add_food_layout.findViewById(R.id.btn_select);
        btn_upload = add_food_layout.findViewById(R.id.btn_upload);
        et_UploadStatus = add_food_layout.findViewById(R.id.tv_uploadStatus);

        alertDialog.setView(add_food_layout);
        alertDialog.setIcon(R.drawable.ic_playlist_add_black_24dp);


        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                uploadImage();
            }
        });
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChoseImage();
            }
        });

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (newFood != null) {
                    FoodList.push().setValue(newFood);
                }
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();

    }

    private void LoadItems(String catID) {
        adapter = new FirebaseRecyclerAdapter<Food, FoodViewHolder>(Food.class,
                R.layout.food_list,
                FoodViewHolder.class,
                FoodList.orderByChild("menuId").equalTo(catID)) {

            @Override
            protected void populateViewHolder(FoodViewHolder viewHolder, Food model, int position) {
                viewHolder.tv_food_name.setText(model.getName());
                Picasso.with(getBaseContext()).load(model.getImage()).into(viewHolder.img_food);

                final Food clickitem = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //   Toast.makeText(FoodList.this, "" + clickitem.getName() + " is Clicked", Toast.LENGTH_SHORT).show();
                        //Get Catagory ID and pass it to new Activity;
                        //Start new Activity for selected item
                        //  Intent itemDetails = new Intent(FoodList.this, FoodDetails.class);
                        //  itemDetails.putExtra("foodID", adapter.getRef(position).getKey());
                        //   startActivity(itemDetails);
//
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

    }

    private void ChoseImage() {
        //Lets the user select an image from galery and save the Uri created in the firebase Storage
        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST &&
                resultCode == RESULT_OK &&
                data != null &&
                data.getData() != null) {
            saveUri = data.getData();
            btn_select.setText("Image Selected !");
            btn_select.setActivated(false);
        }

    }

    private void uploadImage() {
        if (saveUri != null) {
            final ProgressDialog mDialog = new ProgressDialog(this);
            mDialog.setMessage("Uploading...");
            mDialog.show();
            String ImageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("Images/" + ImageName);
            imageFolder.putFile(saveUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mDialog.dismiss();
                    et_UploadStatus.setText("Image Uploaded");
                    Toast.makeText(FoodList.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            newFood = new Food();
                            newFood.setName(edtName.getText().toString());
                            newFood.setImage(uri.toString());
                            newFood.setDescription(etDiscription.getText().toString());
                            newFood.setDiscount(etDiscount.getText().toString());
                            newFood.setMenuId(CatID);
                            newFood.setPrice(etPrice.getText().toString());
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mDialog.dismiss();
                    Toast.makeText(FoodList.this, "Uploading Failed", Toast.LENGTH_SHORT).show();
                    Log.d("HomeActivity", "onFailure: " + e.toString());
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    mDialog.setMessage("Uploaded" + progress + " %");
                }
            });
        }
    }

    //UPDATE | DELETE Category
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.UPDATE)) {
            showUpdateDialog(adapter.getRef(item.getOrder()).getKey(), adapter.getItem(item.getOrder()));
        } else if (item.getTitle().equals(Common.DELETE)) {
            DeleteCategory(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);

    }

    private void DeleteCategory(String key) {
        FoodList.child(key).removeValue();
        Toast.makeText(FoodList.this, "Food Deleted", Toast.LENGTH_SHORT).show();
    }

    private void showUpdateDialog(final String key, @org.jetbrains.annotations.NotNull final Food item) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Update Category");
        alertDialog.setMessage("Please Fill All information");

        LayoutInflater inflater = this.getLayoutInflater();

        View Edit_Food_layout = inflater.inflate(R.layout.add_new_food_layout, null);

        edtName = Edit_Food_layout.findViewById(R.id.et_NewFoodName);
        etDiscription = Edit_Food_layout.findViewById(R.id.et_Description);
        etPrice = Edit_Food_layout.findViewById(R.id.et_Price);
        etDiscount = Edit_Food_layout.findViewById(R.id.et_Discount);
        btn_select = Edit_Food_layout.findViewById(R.id.btn_select);
        btn_upload = Edit_Food_layout.findViewById(R.id.btn_upload);
        et_UploadStatus = Edit_Food_layout.findViewById(R.id.tv_uploadStatus);

        btn_select = Edit_Food_layout.findViewById(R.id.btn_select);
        btn_upload = Edit_Food_layout.findViewById(R.id.btn_upload);
        alertDialog.setView(Edit_Food_layout);
        alertDialog.setIcon(R.drawable.ic_menu_share);

        //Default Name
        edtName.setText(item.getName());

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChoseImage();
            }
        });
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeImage(item);
            }
        });

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                item.setName(edtName.getText().toString());
                FoodList.child(key).setValue(item);
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    public void ChangeImage(final Food item) {
        if (saveUri != null) {
            final ProgressDialog mDialog = new ProgressDialog(this);
            mDialog.setMessage("Uploading...");
            mDialog.show();
            String ImageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("Images/" + ImageName);
            imageFolder.putFile(saveUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mDialog.dismiss();
                    Toast.makeText(FoodList.this, "Uploaded", Toast.LENGTH_SHORT).show();
                    imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            item.setImage(uri.toString());
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mDialog.dismiss();
                    Toast.makeText(FoodList.this, "Uploading Failed", Toast.LENGTH_SHORT).show();
                    Log.d("HomeActivity", "onFailure: " + e.toString());
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    mDialog.setMessage("Uploaded" + progress + " %");
                }
            });
        }
    }
}
