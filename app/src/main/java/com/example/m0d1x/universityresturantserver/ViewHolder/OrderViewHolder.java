package com.example.m0d1x.universityresturantserver.ViewHolder;

import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.m0d1x.universityresturantserver.Common.Common;
import com.example.m0d1x.universityresturantserver.Interface.ItemClickListener;
import com.example.m0d1x.universityresturantserver.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OrderViewHolder extends
        RecyclerView.ViewHolder {


    public TextView tv_OrderID, tv_OorderStatus, tv_Order_Phone, tv_Oorder_Address;
    public Button btn_Edit, btn_Remove, btn_Details, btn_Directions;
    private ItemClickListener itemClickListener;

    public OrderViewHolder(@NonNull View itemView) {
        super(itemView);

        tv_OrderID = (TextView) itemView.findViewById(R.id.order_id);
        tv_OorderStatus = (TextView) itemView.findViewById(R.id.order_status);
        tv_Order_Phone = (TextView) itemView.findViewById(R.id.order_phone);
        tv_Oorder_Address = (TextView) itemView.findViewById(R.id.order_Address);
        btn_Edit = (Button) itemView.findViewById(R.id.btn_Edit);
        btn_Remove = (Button) itemView.findViewById(R.id.btn_Remove);
        btn_Details = (Button) itemView.findViewById(R.id.btn_Details);
        btn_Directions = (Button) itemView.findViewById(R.id.btn_Directions);


    }


}



