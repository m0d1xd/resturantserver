package com.example.m0d1x.universityresturantserver.ViewHolder;

import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.m0d1x.universityresturantserver.R;
import com.example.m0d1x.universityresturantserver.model.Food;
import com.example.m0d1x.universityresturantserver.model.Order;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


class OrderDetailsViewHolder extends RecyclerView.ViewHolder {

    public TextView prod_name, prod_price, prod_discount, prod_quantity;

    public ImageView product_img;

    public OrderDetailsViewHolder(@NonNull View itemView) {
        super(itemView);
        prod_name = (TextView) itemView.findViewById(R.id.product_name);
        prod_price = (TextView) itemView.findViewById(R.id.product_price);
        prod_discount = (TextView) itemView.findViewById(R.id.product_discount);
        prod_quantity = (TextView) itemView.findViewById(R.id.product_quantity);
        product_img = (ImageView) itemView.findViewById(R.id.product_img);

    }
}

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsViewHolder> {

    List<Order> mylist;

    public OrderDetailsAdapter(List<Order> mylist) {

        this.mylist = mylist;
    }

    @NonNull
    @Override
    public OrderDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_details_layout, parent, false);

        return new OrderDetailsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailsViewHolder holder, int position) {
        Order order = mylist.get(position);

        holder.prod_name.setText(String.format("Name : %s", order.getProductName()));
        holder.prod_quantity.setText(String.format("Quantity : %s", order.getQuantity()));
        holder.prod_discount.setText(String.format("Discount : %s", order.getDiscount()));
        holder.prod_price.setText(String.format("Price  : %s", order.getPrice()));

    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }
}
