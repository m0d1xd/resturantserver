package com.example.m0d1x.universityresturantserver.model;

public class CrossImgID {
    String id;
    String url;

    public CrossImgID() {
    }

    public CrossImgID(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
