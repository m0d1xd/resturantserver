package com.example.m0d1x.universityresturantserver.Common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.example.m0d1x.universityresturantserver.Remote.APIService;
import com.example.m0d1x.universityresturantserver.Remote.IGeoCoordinates;
import com.example.m0d1x.universityresturantserver.Remote.IGoogleService;
import com.example.m0d1x.universityresturantserver.Remote.RetrofitClient;
import com.example.m0d1x.universityresturantserver.model.Request;
import com.example.m0d1x.universityresturantserver.model.User;

public class Common {
    public static User CurrentUser;
    public static Request CurrentRequest;
    public static final String UPDATE = "Update";
    public static final String DELETE = "Delete";


    public static final String BASE_URL = "https://fcm.googleapis.com/";
    public static final String GOOGLE_API_URL = "https://maps.googleapis.com/";


    public static IGoogleService getGoogleMapAPI() {
        return RetrofitClient.getGoogleClient(GOOGLE_API_URL).create(IGoogleService.class);
    }

    public static IGeoCoordinates getGeoCodeService() {

        return RetrofitClient.getClient(GOOGLE_API_URL).create(IGeoCoordinates.class);
    }


    public static String convertCodeToString(String code) {
        switch (code) {
            case "0":
                return "Placed";
            case "1":
                return "On My Way";
            case "2":
                return "Shipped";
            default:
                return "UnKnown";
        }
    }

    public static APIService getFCMService() {
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHight) {

        Bitmap scaleBitmap = Bitmap.createBitmap(newWidth, newHight, Bitmap.Config.ARGB_8888);
        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHight / (float) bitmap.getHeight();
        float pivotX = 0, pivotY = 0;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);


        Canvas canvas = new Canvas(scaleBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));


        return scaleBitmap;
    }
}
