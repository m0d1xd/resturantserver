package com.example.m0d1x.universityresturantserver.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.m0d1x.universityresturantserver.Activities.Orders.FoodList;
import com.example.m0d1x.universityresturantserver.Common.Common;
import com.example.m0d1x.universityresturantserver.Interface.ItemClickListener;
import com.example.m0d1x.universityresturantserver.R;
import com.example.m0d1x.universityresturantserver.ViewHolder.FoodViewHolder;
import com.example.m0d1x.universityresturantserver.ViewHolder.MenuViewHolder;
import com.example.m0d1x.universityresturantserver.ViewHolder.OrderDetailsAdapter;
import com.example.m0d1x.universityresturantserver.ViewHolder.Test;
import com.example.m0d1x.universityresturantserver.model.Category;
import com.example.m0d1x.universityresturantserver.model.CrossImgID;
import com.example.m0d1x.universityresturantserver.model.Food;
import com.example.m0d1x.universityresturantserver.model.Order;
import com.example.m0d1x.universityresturantserver.model.Request;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrderDetails extends AppCompatActivity {


    //View Elements
    TextView order_id, order_name, order_total, order_Address, order_comment;
    RecyclerView lstFood;
    RecyclerView.LayoutManager layoutManager;
    String order_id_value = "";
    FirebaseRecyclerAdapter<Request, Test> adapter;
    ArrayList<Food> imgIDS;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        order_id = (TextView) findViewById(R.id.order_id);
        order_name = (TextView) findViewById(R.id.order_name);
        order_total = (TextView) findViewById(R.id.order_total);
        order_Address = (TextView) findViewById(R.id.order_Address);
        order_comment = (TextView) findViewById(R.id.order_comment);

        lstFood = (RecyclerView) findViewById(R.id.lstFood);
        lstFood.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        lstFood.setLayoutManager(layoutManager);

        if (getIntent() != null) {
            order_id_value = getIntent().getStringExtra("OrderID");
        }
        order_id.setText(order_id_value);
        order_name.setText(Common.CurrentRequest.getName());
        order_total.setText(Common.CurrentRequest.getTotal());
        order_Address.setText(Common.CurrentRequest.getAddress());
        order_comment.setText(Common.CurrentRequest.getComment());

        imgIDS = GetImaged();

        OrderDetailsAdapter adapter = new OrderDetailsAdapter(Common.CurrentRequest.getFoods());

        adapter.notifyDataSetChanged();

        lstFood.setAdapter(adapter);

    }

    public ArrayList<String> Images(List<Order> Foods) {
        ArrayList<String> ImagesUrl = new ArrayList<>();
        DatabaseReference db = FirebaseDatabase.getInstance().getReference("Foods");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return ImagesUrl;
    }


    public ArrayList<Food> GetImaged() {
        final ArrayList<Food> imgIDS = new ArrayList<>();

        DatabaseReference db = FirebaseDatabase.getInstance().getReference("Foods");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot post : dataSnapshot.getChildren()) {
                    Food f = post.getValue(Food.class);
                    Log.d("Images", post.getKey() + "onDataChange: " + f.getImage());
                    imgIDS.add(f);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return imgIDS;

    }
}
